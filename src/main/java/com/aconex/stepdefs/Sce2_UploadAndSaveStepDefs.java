package com.aconex.stepdefs;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aconex.common.CommonMethods;
import com.aconex.common.ExtentFactory;
import com.aconex.common.FileLocation;
import com.aconex.pages.UploadPage;
import com.aconex.runner.Runcukes;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class Sce2_UploadAndSaveStepDefs extends Runcukes {

	private String randomDocumnetID = CommonMethods.getRandomID(5);

	@Then("User uploads new document$")
	public void User_uploads_new_document() throws InterruptedException, Exception, IOException {
		String absPath = FileLocation.getPath();
		report = ExtentFactory.getInstance(absPath);
		testCaseName = report.startTest("User uploads new document");
		finalExtentTest = testCaseName;

		try {
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			CommonMethods.waitForLoad(driver);
			// Click on document
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, UploadPage.documentButton, 10);
			CommonMethods.scrollToElement(driver, UploadPage.documentButton, "100");
			CommonMethods.clickElement(driver, UploadPage.documentButton, 30);
			testCaseName.log(LogStatus.PASS, "Clicked on Document");

			// Click on
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, UploadPage.uploadaDocumentButton, 10);
			CommonMethods.scrollToElement(driver, UploadPage.uploadaDocumentButton, "100");
			CommonMethods.clickElement(driver, UploadPage.uploadaDocumentButton, 30);
			testCaseName.log(LogStatus.PASS, "Clicked on upload a new document");

			// driver.switchTo().frame("main");
			// Thread.sleep(3000);

			// enter doc number
			new WebDriverWait(driver, 30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("frameMain"));
			WebElement docElement = driver.findElement(By.xpath(UploadPage.documentNoTextBox));
			docElement.clear();
			docElement.sendKeys(randomDocumnetID);
			testCaseName.log(LogStatus.PASS, "Entered document number as : " + randomDocumnetID);

			// new WebDriverWait(driver,
			// 30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("frameMain"));
			WebElement revisionElement = driver.findElement(By.xpath(UploadPage.revisionTextBox));
			revisionElement.clear();
			String randomrevisionElement = CommonMethods.getRandomID(5);
			revisionElement.sendKeys(randomrevisionElement);
			testCaseName.log(LogStatus.PASS, "Entered revision number as : " + randomrevisionElement);

			// new WebDriverWait(driver,
			// 30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("frameMain"));
			WebElement titleElement = driver.findElement(By.xpath(UploadPage.titleTextBox));
			titleElement.clear();
			String randomtitleElement = CommonMethods.getRandomString(5);
			titleElement.sendKeys(randomtitleElement);
			testCaseName.log(LogStatus.PASS, "Entered title as : " + randomtitleElement);

			// new WebDriverWait(driver,
			// 30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("frameMain"));
			Select type = new Select(driver.findElement(By.xpath(UploadPage.typeDropDown)));
			type.selectByIndex(1);
			testCaseName.log(LogStatus.PASS, "Selected type  ");

			// new WebDriverWait(driver,
			// 30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("frameMain"));
			Select status = new Select(driver.findElement(By.xpath(UploadPage.statusDropDown)));
			status.selectByIndex(1);
			testCaseName.log(LogStatus.PASS, "Selected status  ");

			// new WebDriverWait(driver,
			// 30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("frameMain"));
			Select discipline = new Select(driver.findElement(By.xpath(UploadPage.disciplineDropDown)));
			discipline.selectByIndex(1);
			testCaseName.log(LogStatus.PASS, "Selected discipline  ");

			Thread.sleep(4000);
			String workingDir = System.getProperty("user.dir");
			String filePath = "/dataFiles/testDoc.txt";
			String uploadPath = workingDir + filePath;
			WebElement uploadElement = driver.findElement(By.xpath(UploadPage.clickToUploadLink));
			CommonMethods.sendKeys(uploadElement, uploadPath);
			testCaseName.log(LogStatus.PASS, "Uploaded file");

			WebElement dateElement = driver.findElement(By.xpath(UploadPage.revisionDate));
			dateElement.click();
			String strdateElement = "18/11/2020";
			dateElement.sendKeys(strdateElement); // can be sent from config
			testCaseName.log(LogStatus.PASS, "Entered date : " + strdateElement);

			/*
			 * String workingDir = System.getProperty("user.dir"); String filePath =
			 * "/AutoIT/Fileupload.exe"; String uploadPath = workingDir + filePath;
			 * Runtime.getRuntime().exec(uploadPath); Thread.sleep(4000);
			 * Runtime.getRuntime().exec("C:/Users/snidagundi/Desktop/AutoIT/Fileupload.exe"
			 * );
			 */

			// click on upload after entering data
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, UploadPage.uploadFinalButton, 10);
			CommonMethods.scrollToElement(driver, UploadPage.uploadFinalButton, "100");
			CommonMethods.clickElement(driver, UploadPage.uploadFinalButton, 30);
			testCaseName.log(LogStatus.PASS, "Clicked on upload document");
			WebElement generatedDocumentNumber = driver.findElement(By.xpath(UploadPage.documentNameOnSuccessPage));
			String strGeneratedReferenceNumber = generatedDocumentNumber.getText();

			// check success message & document name disaplayed along with message
			// if doc ID found, verify against doc ID which was sent while creating
			if (CommonMethods.verifyElementDisplayed(driver, UploadPage.successMessage, 20)
					&& CommonMethods.verifyElementDisplayed(driver, UploadPage.documentNameOnSuccessPage, 20)) {
				if (randomDocumnetID.equalsIgnoreCase(strGeneratedReferenceNumber)) {
					testCaseName.log(LogStatus.PASS,
							"The following document has been uploaded to organization's document register : "
									+ strGeneratedReferenceNumber);
					Assert.assertTrue(true);
				} else {
					testCaseName.log(LogStatus.FAIL, "Document number mismatched, actual : "
							+ strGeneratedReferenceNumber + " Expected :" + randomDocumnetID);
					Assert.assertTrue(false);
				}
			} else {
				testCaseName.log(LogStatus.FAIL, "Document upload failed");
				Assert.assertTrue(false);
			}

		} catch (Exception e) {
			testCaseName.log(LogStatus.FAIL, ExceptionUtils.getStackTrace(e).substring(0, 300));
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("user.dir") + "/target/Reports/" + dateName + ".png";
			File finalDestination = new File(destination);
			FileHandler.copy(source, finalDestination);

			e.printStackTrace();

			report.endTest(testCaseName);
			report.flush();
		}

	}

	@Then("User searches document$")
	public void User_searches_document() throws InterruptedException, Exception, IOException {
		String absPath = FileLocation.getPath();
		report = ExtentFactory.getInstance(absPath);
		testCaseName = report.startTest("User searches document");
		finalExtentTest = testCaseName;

		try {
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			// Clicked on view in Document Register link
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, UploadPage.viewInDocumentRegisterLink, 20);
			CommonMethods.scrollToElement(driver, UploadPage.viewInDocumentRegisterLink, "100");
			CommonMethods.clickElement(driver, UploadPage.viewInDocumentRegisterLink, 30);
			testCaseName.log(LogStatus.PASS, "Clicked on view in Document Register link ");

			// search doc
			WebElement docSearchElement = driver.findElement(By.xpath(UploadPage.searchDocumentTextBox));
			docSearchElement.clear();
			docSearchElement.sendKeys(randomDocumnetID);
			testCaseName.log(LogStatus.PASS, "Entered document name in search box : " + randomDocumnetID);

			WebElement searchButtonElement = driver.findElement(By.xpath(UploadPage.searchDocumentButton));
			searchButtonElement.click();
			testCaseName.log(LogStatus.PASS, "Clicked on search button");

			// Check if search result fetches record, get first record value
			// if first record found verify against documnet ID which was sent while
			// creating
			// few other checks can be added here.
			if (CommonMethods.verifyElementDisplayed(driver, UploadPage.firstRecord, 30)) {
				WebElement generatedDocumentNumber = driver.findElement(By.xpath(UploadPage.firstRecord));
				String strGeneratedReferenceNumber = generatedDocumentNumber.getText();

				if (strGeneratedReferenceNumber.equalsIgnoreCase(randomDocumnetID)) {
					testCaseName.log(LogStatus.PASS, "Searched document is verified with document found in result set :"
							+ strGeneratedReferenceNumber);
					Assert.assertTrue(true);
				}
			} else {
				testCaseName.log(LogStatus.FAIL, "Document not found : " + randomDocumnetID);
				Assert.assertTrue(false);
			}
		}

		catch (Exception e) {
			testCaseName.log(LogStatus.FAIL, ExceptionUtils.getStackTrace(e).substring(0, 300));
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("user.dir") + "/target/Reports/" + dateName + ".png";
			File finalDestination = new File(destination);
			FileHandler.copy(source, finalDestination);

			e.printStackTrace();

			report.endTest(testCaseName);
			report.flush();
		}

	}

	@And("User saves search criteria$")
	public void User_saves_search() throws InterruptedException, Exception, IOException {
		String absPath = FileLocation.getPath();
		report = ExtentFactory.getInstance(absPath);
		testCaseName = report.startTest("User saves search criteria");
		finalExtentTest = testCaseName;

		try {
			// Clicked save search
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, UploadPage.saveSearchAs, 20);
			CommonMethods.scrollToElement(driver, UploadPage.saveSearchAs, "100");
			CommonMethods.clickElement(driver, UploadPage.saveSearchAs, 30);
			testCaseName.log(LogStatus.PASS, "Clicked on save search as ");

			// Search document
			WebElement nameTextFieldElement = driver.findElement(By.xpath(UploadPage.nameTextField));
			nameTextFieldElement.clear();
			nameTextFieldElement.sendKeys(randomDocumnetID);
			testCaseName.log(LogStatus.PASS, "Entered save as name in text box : " + randomDocumnetID);

			WebElement saveSearchCriteriaBtn = driver.findElement(By.xpath(UploadPage.saveSearchCriteriaButton));
			saveSearchCriteriaBtn.click();
			testCaseName.log(LogStatus.PASS, "Clicked on save search criteria ");

			// verify success message disaplays
			// few other checks can be added here
			if (CommonMethods.verifyElementDisplayed(driver, UploadPage.successMessageUponSaving, 30)) {
				testCaseName.log(LogStatus.PASS, "Saved search criteria with name : " + randomDocumnetID);
				Assert.assertTrue(true);
			} else {
				testCaseName.log(LogStatus.FAIL, "Could not save search criteria");
				Assert.assertTrue(false);
			}
		}

		catch (Exception e) {
			testCaseName.log(LogStatus.FAIL, ExceptionUtils.getStackTrace(e).substring(0, 300));
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("user.dir") + "/target/Reports/" + dateName + ".png";
			File finalDestination = new File(destination);
			FileHandler.copy(source, finalDestination);

			e.printStackTrace();

			report.endTest(testCaseName);
			report.flush();
		}

	}
}