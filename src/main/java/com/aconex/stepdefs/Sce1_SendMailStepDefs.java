package com.aconex.stepdefs;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aconex.common.CommonMethods;
import com.aconex.common.ExtentFactory;
import com.aconex.common.FileLocation;
import com.aconex.pages.HomePage;
import com.aconex.pages.LoginPage;
import com.aconex.runner.Runcukes;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Sce1_SendMailStepDefs extends Runcukes {

	@When("User enters valid credentials to login \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void User_enters_valid_credentials_to_login(String userName, String password, String pageTitle)
			throws InterruptedException, Exception, IOException {

		String absPath = FileLocation.getPath();
		report = ExtentFactory.getInstance(absPath);
		testCaseName = report.startTest("Login to Aconix");
		finalExtentTest = testCaseName;

		try {

			driver.navigate().refresh();
			CommonMethods.waitForLoad(driver);
			// Enter username
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, LoginPage.userNameTextBox, 10);
			CommonMethods.scrollToElement(driver, LoginPage.userNameTextBox, "100");
			CommonMethods.sendKeysElement(driver, LoginPage.userNameTextBox, config.getProperty(userName), 10);
			testCaseName.log(LogStatus.PASS, "Entered username");

			// Enter password
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, LoginPage.passwordTextBox, 10);
			CommonMethods.scrollToElement(driver, LoginPage.passwordTextBox, "100");
			CommonMethods.sendKeysElement(driver, LoginPage.passwordTextBox, config.getProperty(password), 10);
			testCaseName.log(LogStatus.PASS, "Entered Password");

			// Click on login button
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, LoginPage.loginBtn, 10);
			CommonMethods.scrollToElement(driver, LoginPage.loginBtn, "100");
			CommonMethods.clickElement(driver, LoginPage.loginBtn, 30);
			testCaseName.log(LogStatus.PASS, "Clicked on login button");

			// Verify login
			if (CommonMethods.verifyElementDisplayed(driver, HomePage.mailButton, 20)
					&& CommonMethods.verifyElementDisplayed(driver, HomePage.oracleLogo, 20))

			{
				testCaseName.log(LogStatus.PASS, "User logged in successfully to Aconex");
				Assert.assertTrue(true);
			}

			else {
				testCaseName.log(LogStatus.FAIL, "User login unsuccessful");
				Assert.assertTrue(false);
			}

			// Verify page title
			String actualPageTitle = driver.getTitle();
			System.out.println("Page title: " + actualPageTitle);
			if (actualPageTitle.equalsIgnoreCase(config.getProperty(pageTitle)))

			{
				testCaseName.log(LogStatus.PASS, "Verifed the page title : " + actualPageTitle);
				Assert.assertTrue(true);
			} else {
				testCaseName.log(LogStatus.FAIL,
						"Page title does't match : Actual :" + actualPageTitle + " Expected title : " + pageTitle);
				Assert.assertTrue(false);
			}

		} catch (Exception e) {
			testCaseName.log(LogStatus.FAIL, ExceptionUtils.getStackTrace(e).substring(0, 300));
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("user.dir") + "/target/Reports/" + dateName + ".png";
			File finalDestination = new File(destination);
			FileHandler.copy(source, finalDestination);

			e.printStackTrace();

			report.endTest(testCaseName);
			report.flush();
		}

	}

	@Then("User composes new mail and send$")
	public void User_composes_new_mail_and_send() throws InterruptedException, Exception, IOException {
		String absPath = FileLocation.getPath();
		report = ExtentFactory.getInstance(absPath);
		testCaseName = report.startTest("Send mail");
		finalExtentTest = testCaseName;

		try {
			CommonMethods.waitForLoad(driver);
			// Click on mail
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.mailButton, 10);
			CommonMethods.scrollToElement(driver, HomePage.mailButton, "100");
			CommonMethods.clickElement(driver, HomePage.mailButton, 30);
			testCaseName.log(LogStatus.PASS, "User clicked on mail button");

			// Click on blank mail
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.blankMailOption, 10);
			CommonMethods.scrollToElement(driver, HomePage.blankMailOption, "100");
			CommonMethods.clickElement(driver, HomePage.blankMailOption, 30);
			testCaseName.log(LogStatus.PASS, "User clicked on blank mail button");

			// driver.switchTo().frame("main");
			// Thread.sleep(3000);
			new WebDriverWait(driver, 30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("frameMain"));
			driver.findElement(By.id("Correspondence_correspondenceTypeID")).click();
			List<WebElement> allList = driver.findElements(By.xpath("//optgroup//option"));
			allList.get(1).click();
			testCaseName.log(LogStatus.PASS, "User selects on type ID");

			// Select mail type

			// driver.findElements(select[@id='xyz']/optgroup[@label='Group1']/option"));

			// new
			// Select(driver.findElement(By.id("Correspondence_correspondenceTypeID"))).selectByVisibleText("Design
			// Change Notification");

			// driver.findElement(By.xpath(HomePage.mailTypeDropDown)).sendKeys(Keys.DOWN);
			/*
			 * Select mailType = new
			 * Select(driver.findElement(By.xpath(HomePage.mailTypeDropDown)));
			 * mailType.selectByIndex(4);
			 */
			// Select mailType = new
			// Select(driver.findElement(By.xpath(HomePage.mailTypeDropDown)));
			// mailType.selectByVisibleText("Select");

			/*
			 * CommonMethods.waitUntilVisibilityOfElementByXpath(driver,HomePage.
			 * mailTypeDropDown, 10);
			 * CommonMethods.scrollToElement(driver,HomePage.mailTypeDropDown, "100");
			 * CommonMethods.clickElement(driver, HomePage.mailTypeDropDown, 30);
			 */

			// Select to list
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.directoryButton, 10);
			CommonMethods.scrollToElement(driver, HomePage.directoryButton, "100");
			CommonMethods.clickElement(driver, HomePage.directoryButton, 30);
			testCaseName.log(LogStatus.PASS, "User clicks on directory");

			// Select global link
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.globalOptionLink, 10);
			CommonMethods.scrollToElement(driver, HomePage.globalOptionLink, "100");
			CommonMethods.clickElement(driver, HomePage.globalOptionLink, 30);
			testCaseName.log(LogStatus.PASS, "User navigates to global option link");

			// group name
			String grouName = "Kenton";
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.groupName, 10);
			CommonMethods.scrollToElement(driver, HomePage.groupName, "100");
			CommonMethods.sendKeysElement(driver, HomePage.groupName, grouName, 10); // can be sent from
																						// config.properties
			testCaseName.log(LogStatus.PASS, "User enters group name : " + grouName);

			// family name
			String familyName = "Tillman";
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.familyName, 10);
			CommonMethods.scrollToElement(driver, HomePage.familyName, "100");
			CommonMethods.sendKeysElement(driver, HomePage.familyName, familyName, 10); // can be sent from
																						// config.properties
			testCaseName.log(LogStatus.PASS, "User enters family name : " + familyName);

			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.searchButton, 10);
			CommonMethods.scrollToElement(driver, HomePage.searchButton, "100");
			CommonMethods.clickElement(driver, HomePage.searchButton, 30);
			testCaseName.log(LogStatus.PASS, "User clicked on search button");

			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.selectNamecheckBox, 10);
			CommonMethods.scrollToElement(driver, HomePage.selectNamecheckBox, "100");
			CommonMethods.clickElement(driver, HomePage.selectNamecheckBox, 30);
			testCaseName.log(LogStatus.PASS, "User checks name");

			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.toButton, 10);
			CommonMethods.scrollToElement(driver, HomePage.toButton, "100");
			CommonMethods.clickElement(driver, HomePage.toButton, 30);
			testCaseName.log(LogStatus.PASS, "User clicked on OK button");

			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.okBtn, 10);
			CommonMethods.scrollToElement(driver, HomePage.okBtn, "100");
			CommonMethods.clickElement(driver, HomePage.okBtn, 30);

			// Enter subject
			String strSubject = "Test Email";
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.subjectTextBox, 10);
			CommonMethods.scrollToElement(driver, HomePage.subjectTextBox, "100");
			CommonMethods.clickElement(driver, HomePage.subjectTextBox, 30);
			CommonMethods.sendKeysElement(driver, HomePage.subjectTextBox, strSubject, 10); // can be sent from
																							// config.properties
			testCaseName.log(LogStatus.PASS, "User enters on subject name : " + strSubject);

			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.sendbutton, 10);
			CommonMethods.scrollToElement(driver, HomePage.sendbutton, "100");
			CommonMethods.clickElement(driver, HomePage.sendbutton, 30);
			testCaseName.log(LogStatus.PASS, "User clicked on send button");

			// CommonMethods.waitUntilVisibilityOfElementByXpath(driver, , 10);
			String strActualTitle = CommonMethods.getElementText(driver, HomePage.mailTitle, "title", 10);
			WebElement generatedMailNumber = driver.findElement(By.xpath(HomePage.generatedMailNumber));
			String strGeneratedMailNumber = generatedMailNumber.getText();

			WebElement generatedReferenceNumber = driver.findElement(By.xpath(HomePage.generatedReferenceNumber));
			String strGeneratedReferenceNumber = generatedReferenceNumber.getText();

			// check if subject matches
			// check if mail number generated
			// check if reference number generated
			// few other checks can be added here
			if (strActualTitle.equalsIgnoreCase(strSubject)
					&& (CommonMethods.verifyElementDisplayed(driver, HomePage.generatedMailNumber, 30))
					&& (CommonMethods.verifyElementDisplayed(driver, HomePage.generatedReferenceNumber, 30))) {

				testCaseName.log(LogStatus.PASS,
						"Succesfully sent mail and " + " Mail subject :" + strActualTitle + "Mail Number : "
								+ strGeneratedMailNumber + " Reference Number :" + strGeneratedReferenceNumber);
				Assert.assertTrue(true);
			}

			else {

				testCaseName.log(LogStatus.FAIL, "Unable to fetch mail details");
				Assert.assertTrue(false);

			}

		} catch (Exception e) {
			testCaseName.log(LogStatus.FAIL, ExceptionUtils.getStackTrace(e).substring(0, 300));
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("user.dir") + "/target/Reports/" + dateName + ".png";
			File finalDestination = new File(destination);
			FileHandler.copy(source, finalDestination);

			e.printStackTrace();

			report.endTest(testCaseName);
			report.flush();
		}

	}

	@And("User logout$")
	public void User_log_out() throws InterruptedException, Exception, IOException {

		String absPath = FileLocation.getPath();
		report = ExtentFactory.getInstance(absPath);
		testCaseName = report.startTest("Logout");
		finalExtentTest = testCaseName;

		try {

			driver.navigate().refresh();
			CommonMethods.waitForLoad(driver);
			// Enter username
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.userInfo, 10);
			CommonMethods.scrollToElement(driver, HomePage.userInfo, "100");
			CommonMethods.clickElement(driver, HomePage.userInfo, 30);

			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.logOffButton, 10);
			CommonMethods.scrollToElement(driver, HomePage.logOffButton, "100");
			CommonMethods.clickElement(driver, HomePage.logOffButton, 30);
			testCaseName.log(LogStatus.PASS, "User logged out of session");

		} catch (Exception e) {
			testCaseName.log(LogStatus.FAIL, ExceptionUtils.getStackTrace(e).substring(0, 300));
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("user.dir") + "/target/Reports/" + dateName + ".png";
			File finalDestination = new File(destination);
			FileHandler.copy(source, finalDestination);

			e.printStackTrace();

			report.endTest(testCaseName);
			report.flush();
		}

	}

	@Given("User clicks on login$")
	public void User_click_on_login() throws InterruptedException, Exception, IOException {

		String absPath = FileLocation.getPath();
		report = ExtentFactory.getInstance(absPath);
		testCaseName = report.startTest("Login");
		finalExtentTest = testCaseName;

		try {

			// Click on login button
			CommonMethods.waitUntilVisibilityOfElementByXpath(driver, HomePage.logInButton, 10);
			CommonMethods.scrollToElement(driver, HomePage.logInButton, "100");
			CommonMethods.clickElement(driver, HomePage.logInButton, 30);
			testCaseName.log(LogStatus.PASS, "Clicked on login button");

		} catch (Exception e) {
			testCaseName.log(LogStatus.FAIL, ExceptionUtils.getStackTrace(e).substring(0, 300));
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("user.dir") + "/target/Reports/" + dateName + ".png";
			File finalDestination = new File(destination);
			FileHandler.copy(source, finalDestination);

			e.printStackTrace();

			report.endTest(testCaseName);
			report.flush();
		}

	}

}
