package com.aconex.stepdefs;

import com.aconex.common.ExtentFactory;
import com.aconex.common.FileLocation;
import com.aconex.runner.Runcukes;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.java.en.Given;

public class LoginPageStepDefs extends Runcukes {
	@Given("^Validate browser launch$")
	public void Validate_browser_launch() {
		String absPath = FileLocation.getPath();
		report = ExtentFactory.getInstance(absPath);
		testCaseName = report.startTest("Launch Aconex URL");
		finalExtentTest = testCaseName;
		testCaseName.log(LogStatus.PASS, "Aconex URL launched successfully");
		report.endTest(testCaseName);
		report.flush();
	}

}
