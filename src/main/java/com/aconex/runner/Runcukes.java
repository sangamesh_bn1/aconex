package com.aconex.runner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aconex.common.ExcelReader;
import com.cucumber.listener.Reporter;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import io.github.bonigarcia.wdm.WebDriverManager;

@CucumberOptions(features = "src/test/java/com/features/aconexFeatures", glue = { "com.aconex.stepdefs" }, tags = {
		"~@Ignore" }, format = { "pretty", "html:target/cucumber-reports/cucumber-pretty",
				"junit:target/cucumber-reports/Cucumber.xml", "json:target/cucumber-reports/CucumberTestReport.json",
				"rerun:target/cucumber-reports/re-run.txt",
				"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html" })

public class Runcukes {
	public String appenv;
	// public String country;
	public String siteUrl;
	public static WebDriver driver;
	private TestNGCucumberRunner testRunner;
	public static Properties config = null;
	public static Properties testdata = null;
	public static FileOutputStream out = null;
	public static String ScenarioName = null;
	public static ExtentTest finalExtentTest;
	public static ExtentTest testCaseName;
	public static ExtentReports report;
	ExtentTest signIn;
	public static SoftAssert softAssert;
	public static String StepName = null;

	public void LoadConfigProperty() throws IOException {
		config = new Properties();
		FileInputStream ip = new FileInputStream("src/main/resources/config.properties");
		config.load(ip);
	}

	@Parameters("browser")
	public void WriteTestDataFile(String browser) throws IOException {
		LoadConfigProperty();
		try {
			appenv = System.getProperty("URL");
			siteUrl = System.getProperty("SITEURL");
			if (appenv.isEmpty() || siteUrl.isEmpty()) { // if (appenv.isEmpty() || country.isEmpty() ||
															// siteUrl.isEmpty()) {
				appenv = config.getProperty("URL");
				siteUrl = config.getProperty("SiteURL");
			}

			ExcelReader xlreader = new ExcelReader();
			int rowid = xlreader.getTCRowIndex("TestData", appenv);
			xlreader.getTCInputData(appenv, rowid); // xlreader.getTCInputData(appenv, country, rowid);
			HashMap<String, String> testData = xlreader.getTCInputData(appenv, rowid);
			String fileName = "src/main/resources/config.properties";
			File myfile = new File(fileName);
			PrintWriter printer = new PrintWriter(myfile);
			printer.print("");
			printer.close();
			FileWriter writer = new FileWriter(myfile, true);

			for (String name : testData.keySet()) {
				String key = name.toString();
				String value = testData.get(name).toString();
				if (key.equalsIgnoreCase("SiteURL")) {
					writer.write(key + " = " + siteUrl);
				} else
					writer.write(key + " = " + value);
				writer.write("\r\n");
			}
			writer.write("Browser = " + browser);
			writer.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Parameters({ "browser" })
	@BeforeClass
	public void setupClass(String browser) throws Exception {
		WriteTestDataFile(browser);
		testRunner = new TestNGCucumberRunner(this.getClass());
		System.out.println("Starting the suite");
		browserInitialization(browser);
		selectAppEnv();
	}

	@Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
	public void feature(CucumberFeatureWrapper cucumberFeature) throws Exception {
		softAssert = new SoftAssert();
		testRunner.runCucumber(cucumberFeature.getCucumberFeature());
		softAssert.assertAll();
	}

	@DataProvider(name = "features")
	public Object[][] getFeatures() {
		return testRunner.provideFeatures();
	}

	@AfterMethod
	public void after(ITestResult testResult, final ITestContext testContext) throws Exception {
		if (testResult.getStatus() == ITestResult.SUCCESS) {
			softAssert.assertAll();
			System.out.println("***testResult.getStatus()****" + testResult.getStatus() + "**ITestResult.SUCCESS**"
					+ ITestResult.SUCCESS);
			report.endTest(testCaseName);
			report.flush();

		} else if (testResult.getStatus() == ITestResult.FAILURE) {
			softAssert.assertAll();
			System.out.println("***testResult.getStatus()****" + testResult.getStatus() + "**ITestResult.FAILURE**"
					+ ITestResult.FAILURE);

			Properties config = new Properties();
			FileInputStream ip = new FileInputStream("src/main/resources/config.properties");
			config.load(ip);
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("user.dir") + "/target/Reports/" + dateName + ".png";
			File finalDestination = new File(destination);
			FileHandler.copy(source, finalDestination);
			String imagePath = finalExtentTest.addScreenCapture(destination);
			finalExtentTest.log(LogStatus.FAIL, "Failed Test Case", imagePath);

			report.endTest(testCaseName);
			report.flush();
		} else if (testResult.getStatus() == ITestResult.SKIP) {
			softAssert.assertAll();
			System.out.println("***testResult.getStatus()****" + testResult.getStatus() + "**ITestResult.SKIP**"
					+ ITestResult.SKIP);
			finalExtentTest.log(LogStatus.SKIP, "Test skipped ");
			report.endTest(testCaseName);
			report.flush();
		}
	}

	@AfterClass
	public void tearDown() {
		Reporter.loadXMLConfig(new File("src/test/java/cucumber/config/extent-config.xml"));
		Reporter.setSystemInfo("Environment", config.getProperty("URL"));
		Reporter.setSystemInfo("Selenium Version", "3.11");
		Reporter.setSystemInfo("Platform", System.getProperty("os.name"));
		Reporter.setSystemInfo("JAVA Version", "1.8");
		Reporter.setSystemInfo("Maven Version", "3.5.3");
		testRunner.finish();
	}

	@AfterSuite(alwaysRun = true)
	public void quit() throws IOException {
		System.out.println("Trying to close the session after suite");
		driver.quit();
	}

	public void selectAppEnv() throws IOException, Exception {

		System.out.println(config.getProperty("SiteURL"));
		driver.get(config.getProperty("SiteURL"));
		System.out.println(
				"************************" + config.getProperty("SiteURL") + "*******************************");
	}

	@SuppressWarnings("deprecation")
	public void browserInitialization(String browser) throws IOException, Exception {
		LoadConfigProperty();
		String exeEnv = config.getProperty("ExecutionEnvironment");
		String osType = System.getProperty("os.name");
		System.out.println(osType);

		if (browser.equalsIgnoreCase("Chrome")) {
			if (exeEnv.equalsIgnoreCase("Local")) {
				if (osType.contains("Win")) {
					WebDriverManager.chromedriver().setup();

					ChromeOptions options = new ChromeOptions();
					options.addArguments("--disable-infobars");
					options.addArguments("--window-size=1920,1080");
					options.addArguments("--disable-extensions");
					options.addArguments("--proxy-server='direct://'");
					options.addArguments("--proxy-bypass-list=*");
					options.addArguments("--start-maximized");
					options.addArguments("--disable-gpu");
					options.addArguments("--disable-dev-shm-usage");
					options.addArguments("--no-sandbox");
					options.addArguments("--ignore-certificate-errors");

					driver = new ChromeDriver(options);
					driver.manage().window().maximize();

				} else if (osType.contains("Lin")) {
					System.out.println("Running on Linux");
					WebDriverManager.chromedriver().setup();
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--headless");
					options.addArguments("--disable-infobars");
					options.addArguments("--disable-notifications");
					options.addArguments("--window-size=1920,1080");
					options.addArguments("--disable-extensions");
					options.addArguments("--proxy-server='direct://'");
					options.addArguments("--proxy-bypass-list=*");
					options.addArguments("--start-maximized");
					options.addArguments("--disable-gpu");
					options.addArguments("--disable-dev-shm-usage");
					options.addArguments("--no-sandbox");
					options.addArguments("--ignore-certificate-errors");

					DesiredCapabilities capabilities = new DesiredCapabilities(options);

					driver = new ChromeDriver(capabilities);
					driver.manage().window().maximize();
				} else {
					System.out.println("No OS found");
				}
			} else if (exeEnv.equalsIgnoreCase("Cloud")) {
			}

		}
	}
}
