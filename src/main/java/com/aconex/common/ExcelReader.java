package com.aconex.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	public FileInputStream file = null;
	public static XSSFSheet sheet = null;
	int columnIndexByCoumnName = 1;
	public static String testDataFile = null;
	public static String sheetName = null;
	public static XSSFWorkbook workbook;
	private XSSFRow row = null;

	private XSSFCell cell = null;
	public String filepath = Appvariables.excelFilePath;

	public ExcelReader() {
		try {
			file = new FileInputStream(filepath);
			System.out.println("Path is :" + file.getFD());
			workbook = new XSSFWorkbook(file);
			sheet = workbook.getSheetAt(0);
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("static-access")
	public HashMap<String, String> getTCInputData(String appEnv, int rowId) throws IOException {
		String ColHeader = null;
		String CellValue = null;
		HashMap<String, String> hmCells = new HashMap<String, String>();
		sheet = workbook.getSheet("TestData");
		row = sheet.getRow(0);
		XSSFRow row1 = sheet.getRow(rowId);
		int col = 1;
		// System.out.println("col" + col + "; " +row1.getCell(0).getStringCellValue());
		if (row1.getCell(0).getStringCellValue().trim().equalsIgnoreCase(appEnv)) {
//			System.out.println(row1.getLastCellNum());
			for (col = 0; col <= row1.getLastCellNum(); col++) {
				try {
					if (row1.getCell(col).getCellType() == cell.CELL_TYPE_STRING) {
						if (row1.getCell(col).getStringCellValue().trim() != null) {
//							System.out.println("col"+col+"; "+row.getCell(col).getStringCellValue());
							ColHeader = row.getCell(col).getStringCellValue().trim();
							CellValue = row1.getCell(col).getStringCellValue().trim();
							hmCells.put(ColHeader, CellValue);
							// System.out.println(ColHeader+" = "+CellValue);
//							hmCells.put(row.getCell(col).getStringCellValue().trim(), row1.getCell(col).getStringCellValue().trim());
						} else {
							continue;
						}
					} else {
						if (String.valueOf(row1.getCell(col).getNumericCellValue()) != null) { //
							// System.out.println("col" + col + "; " +
							// row.getCell(col).getStringCellValue());
							ColHeader = row.getCell(col).getStringCellValue().trim();
							CellValue = String.valueOf(row1.getCell(col).getNumericCellValue()).trim();
							// hmCells.put(ColHeader, CellValue);
							// System.out.println(ColHeader+" = "+CellValue); //
							hmCells.put(ColHeader, CellValue);
						} else {
							continue;
						}
					}
					// String.valueOf(row1.getCell(col).getNumericCellValue()).trim() != null
				} catch (NullPointerException e) {
					continue;
				}
			}
		}
		return hmCells;
	}

	public int getTCRowIndex(String SheetName, String appEnv) {
		int rowIndex = -1;
		sheet = workbook.getSheet(SheetName);
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			row = sheet.getRow(i);
			for (int col = 0; col <= row.getLastCellNum(); i++) {
				try {
					if (row.getCell(col).getStringCellValue().equalsIgnoreCase(appEnv)) {
						rowIndex = row.getCell(col).getRowIndex();
						break;
					}
					break;
				} catch (NullPointerException e) {
					continue;
				}
			}
			if (rowIndex >= 0)
				break;
		}
		return rowIndex;
	}
}
