package com.aconex.common;

public class Appvariables {

	public static final String reportPath = "/target/Reports/";
	public static final String reportFileName = "report.html";
	public static String excelFilePath = "src/main/resources/TestDataSheet.xlsx";
	public static String ConfigPropertyFile = "src/main/resources/config.properties";

}
