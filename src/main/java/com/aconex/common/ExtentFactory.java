package com.aconex.common;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentFactory {
	private static ExtentReports extent;
	
	public static ExtentReports getInstance(String absPath) {
		
		if(extent == null){
			String Path = absPath + Appvariables.reportPath + Appvariables.reportFileName;
			extent = new ExtentReports(Path, true);
			extent
		     .addSystemInfo("Selenium Version", "3.11")
		     .addSystemInfo("Platform", System.getProperty("os.name"))
			 .addSystemInfo("JAVA Version", "1.8")
			 .addSystemInfo("Maven Version", "3.5.3");

		}
		
		return extent;		
	}
}