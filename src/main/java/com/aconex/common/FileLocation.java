package com.aconex.common;

import java.io.File;

public class FileLocation {
	private static String currentDirectory;

	public static String getPath() {
		File file = new File("");
		currentDirectory = file.getAbsolutePath();
		System.setProperty("current.directory", currentDirectory);
		return currentDirectory;
	}
}