package com.aconex.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aconex.runner.Runcukes;
import com.cucumber.listener.Reporter;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class CommonMethods extends Runcukes {
	public static ExtentTest finalExtentTest;
	public static String internalserverErrorflag = "false";
	public static String mainframeErrorflag = "false";
	public static String Errorflag = "false";
	public static String notificationErrorflag = "false";

	public static void scrollToElement(WebDriver driver, String locator, String pixelValue) throws Exception {
		Properties config = new Properties();
		FileInputStream ip = new FileInputStream("src/main/resources/config.properties");
		config.load(ip);
		CommonMethods.waitUntilVisibilityOfElementByXpath(driver, locator, 10);
		WebElement QtyPlusButton = driver.findElement(By.xpath(locator));
		String BrowserName = config.getProperty("Browser");
		if (BrowserName.equalsIgnoreCase("IE")) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", QtyPlusButton);
		} else {
			Actions actions = new Actions(driver);
			actions.moveToElement(QtyPlusButton);
			actions.build().perform();
		}
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		Thread.sleep(1000);
		jse.executeScript("window.scrollBy(0," + pixelValue + ")", "");
		Thread.sleep(500);
	}

	public static void clickElement(WebDriver driver, String locator, int Timeout)
			throws InterruptedException, Exception {

		try {

			if (verifyElementDisplayed(driver, locator, Timeout)) {

				waitUntilElementIsClickableByXpath(driver, locator, Timeout);
				driver.findElement(By.xpath(locator)).click();

			}

		} catch (Exception e) {

			softAssert.assertTrue(false, "Not able to click element with xpath " + locator);
			testCaseName.log(LogStatus.FAIL, "Failed!");
			softAssert.assertTrue(false);

			Properties config = new Properties();
			FileInputStream ip = new FileInputStream("src/main/resources/config.properties");
			config.load(ip);
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("user.dir") + "/target/Reports/" + dateName + ".png";
			File finalDestination = new File(destination);
			FileHandler.copy(source, finalDestination);
			String imagePath = finalExtentTest.addScreenCapture(destination);
			finalExtentTest.log(LogStatus.FAIL, imagePath);
			System.out.println("****************Failed test step details start**************");
			e.printStackTrace();
			System.out.println("***************Failed test step details end***************");
			report.endTest(testCaseName);
			e.printStackTrace();
			report.flush();

		}

	}

	public static void sendKeysElement(WebDriver driver, String locator, String InputValue, int Timeout)
			throws InterruptedException {
		Assert.assertTrue(verifyElementDisplayed(driver, locator, Timeout),
				"Element is not displayed with locator" + locator + "");
		String Value = driver.findElement(By.xpath(locator)).getAttribute("value");
		if (!Value.isEmpty()) {
			driver.findElement(By.xpath(locator)).click();
			driver.findElement(By.xpath(locator)).clear();
		}
		driver.findElement(By.xpath(locator)).sendKeys(InputValue);
	}

	public static void clearText(WebDriver driver, String locator, int Timeout) {
		try {
			Assert.assertTrue(verifyElementDisplayed(driver, locator, Timeout), "Element is not displayed");
			driver.findElement(By.xpath(locator)).clear();
		} catch (Exception e) {
			System.out.println("Element is not displayed with Locator" + locator);
		}
	}

	public static String getElementText(WebDriver driver, String locator, String attribute, int Timeout)
			throws InterruptedException {
		String eleValue = null;
		if (verifyElementDisplayed(driver, locator, Timeout)) {
			if (attribute != null) {
				eleValue = driver.findElement(By.xpath(locator)).getAttribute(attribute);
			} else {
				eleValue = driver.findElement(By.xpath(locator)).getText();
			}
		}
		return eleValue.trim();
	}

	public static boolean verifyElementDisplayed(WebDriver driver, String locator, int Timeout)
			throws InterruptedException {
		boolean status = false;
		try {
			waitUntilVisibilityOfElementByXpath(driver, locator, Timeout);
			if (driver.findElement(By.xpath(locator)).isDisplayed())
				status = true;
		} catch (Exception e) {
			status = false;
		}
		return status;
	}

	public static boolean verifyElementNotDisplayed(WebDriver driver, String locator, int Timeout)
			throws InterruptedException {
		boolean status = false;
		WebDriverWait wait = new WebDriverWait(driver, Timeout);
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
			status = true;
		} catch (NoSuchElementException e) {
			status = false;
		}
		return status;
	}

	public static boolean verifyElementEnabled(WebDriver driver, String locator, int Timeout) throws Exception {
		boolean status = false;
		waitUntilVisibilityOfElementByXpath(driver, locator, Timeout);
		try {
			if (driver.findElement(By.xpath(locator)).isEnabled())
				status = true;
			else
				status = false;
		} catch (Exception e) {
			status = false;
		}
		return status;
	}

	private static final String ALPHA_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public static String getRandomString(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_STRING.length());
			builder.append(ALPHA_STRING.charAt(character));
		}
		return builder.toString();
	}

	private static final String RANDOM_NUM = "0123456789";

	public static String getRandomID(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * RANDOM_NUM.length());
			builder.append(RANDOM_NUM.charAt(character));
		}
		return builder.toString();
	}

	public static void driverWait(WebDriver driver, int waitTime) {
		driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);
	}

	public static void waitForpageLoad(WebDriver driver, int waitTime) {
		driver.manage().timeouts().pageLoadTimeout(waitTime, TimeUnit.SECONDS);
	}

	public static void waitUntilVisibilityOfElementByXpath(WebDriver driver, String locator, int Timeout)
			throws Exception {

		try {
			WebDriverWait wait = new WebDriverWait(driver, Timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));

		} catch (Exception e) {
			System.out.println("&*&&&&&&&" + e + "*&&*^*&^&*");
			e.printStackTrace();

//			Properties config = new Properties();
//			FileInputStream ip = new FileInputStream("src/main/resources/config.properties");
//			config.load(ip);
//			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
//			TakesScreenshot ts = (TakesScreenshot) driver;
//			File source = ts.getScreenshotAs(OutputType.FILE);
//			String destination = System.getProperty("user.dir") + "/target/Reports/" + dateName + ".png";
//			File finalDestination = new File(destination);
//			FileHandler.copy(source, finalDestination);
//			String imagePath = finalExtentTest.addScreenCapture(destination);
//			finalExtentTest.log(LogStatus.FAIL, imagePath);
		}

	}

	public static void waitUntilElementIsClickableByXpath(WebDriver driver, String locator, int Timeout) {
		WebDriverWait wait = new WebDriverWait(driver, Timeout);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
	}

	public static void waitUntilInVisibilityOfElementByXpath(WebDriver driver, String locator, int Timeout) {
		WebDriverWait wait = new WebDriverWait(driver, Timeout);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
	}

	public static void writeConfFile(String scenario, String order) {
		try {

			FileWriter fstream = new FileWriter("src/main/resources/output.txt", true);
			BufferedWriter out = new BufferedWriter(fstream);

			out.write("Date:" + new Date());
			out.newLine();
			out.write("ScenarioName:" + scenario);
			out.newLine();
			out.write("OrderNo.:" + order);
			out.newLine();

			// close buffer writer
			out.close();

		} catch (IOException io) {
			io.printStackTrace();
		}
	}

	public static void addScreenShotToReport(WebDriver driver) throws Exception {
		Properties config = new Properties();
		FileInputStream ip = new FileInputStream("src/main/resources/config.properties");
		config.load(ip);
		// below line is just to append the date format with the screenshot name to
		// avoid duplicate names
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		// after execution, you could see a folder "FailedTestsScreenshots" under src
		// folder
		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/" + dateName + ".png";
		File finalDestination = new File(destination);
		FileHandler.copy(source, finalDestination);
		// Returns the captured file path
		Reporter.addScreenCaptureFromPath(destination);
	}

	public static void waitForBrowserToLoad(WebDriver driver) {
		Boolean readyStateComplete = false;
		for (int i = 0; i < 60; i++) {
			try {
				Thread.sleep(1000);
				if (!readyStateComplete) {
					JavascriptExecutor executor = (JavascriptExecutor) driver;
//					executor.executeScript("window.scrollTo(0, 5");
//					executor.executeScript("window.scrollTo(0, document.body.offsetHeight)");
					readyStateComplete = (Boolean) executor.executeScript("return document.readyState")
							.equals("complete");
					break;
				} else {
					System.out.println(readyStateComplete);
					continue;
				}
			} catch (Exception e) {
				continue;
			}
		}
	}

	public static String getScenarioName() throws Exception {
		Properties config = new Properties();
		FileInputStream ip = new FileInputStream("src/main/resources/config.properties");
		config.load(ip);
		String InitScenarioNumber = config.getProperty("ScenarioNumber");
		int IntScenarioNumber = Integer.parseInt(InitScenarioNumber);
		int ScenarioNumber = IntScenarioNumber + 1;
		String FinalScenarioNumber = Integer.toString(ScenarioNumber);
		String ScenarioID = "Scenario " + FinalScenarioNumber;
		System.out.println("The scenario ID for this test case is :" + ScenarioID);
		FileOutputStream out = new FileOutputStream(Appvariables.ConfigPropertyFile);
		config.setProperty("ScenarioNumber", FinalScenarioNumber);
		config.setProperty("ScenarioID", ScenarioID);
		config.store(out, null);
		out.close();
		return ScenarioID;
	}

	public static void sendKeys(WebElement element, String textValue) {
		element.sendKeys(textValue);
	}

	public static void waitForLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(pageLoadCondition);
	}
}
