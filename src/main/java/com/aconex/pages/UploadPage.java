package com.aconex.pages;

public class UploadPage {

	public static final String documentButton = "//div[@class='bicon ic-DOC']//div";
	public static final String uploadaDocumentButton = "//div[@id='nav-bar-DOC-DOC-NEW']";
	public static final String documentNoTextBox = "//input[@id='docno_0']";
	public static final String revisionTextBox = "//input[@name='revision_0']";
	public static final String titleTextBox = "//input[@name='title_0']";
	public static final String typeDropDown = "//select[@id='doctype_0']";
	public static final String statusDropDown = "//select[@id='docstatus_0']";
	public static final String disciplineDropDown = "//select[@id='discipline_0']";
	public static final String revisionDate = "//input[@name='revisiondate_0_da']";
	public static final String clickToUploadLink = "//div[@class='customFileUploadField ']//input";
	public static final String chooseSourceButton = "//button[@id='FileSourceChooser']//div[@class='uiButton-content']";
	public static final String uploadFinalButton = "//button[@id='btnUploadDocument']//div[@class='uiButton-content']";
	public static final String successMessage = "//li[@class='message success']//h3";
	public static final String documentNameOnSuccessPage = "//li[@class='message success']//b";
	public static final String viewInDocumentRegisterLink = "//div[@class='actionGuide-secondary']//h4[@class='actionGuide-actionTitle textlink']";
	public static final String searchDocumentTextBox = "//input[@id='search-keywords-id']";
	public static final String searchDocumentButton = "//button[@class='auiButton primary ng-binding']";
	public static final String firstRecord ="//div[@col-id='docno'][@role='gridcell']";
	public static final String saveSearchAs ="//div[@class='searchActions-item ng-scope']";
	public static final String nameTextField="//div[@class='form-field']//input[@type='text']";
	public static final String discriptionTextField="//textarea[@class='auiField-input ng-pristine ng-untouched ng-valid ng-empty ng-valid-maxlength']";
	public static final String saveSearchCriteriaButton="//input[@type='submit']";
	public static final String successMessageUponSaving="//div[@class='auiMessage-content']";
	
}
