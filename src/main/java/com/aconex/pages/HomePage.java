package com.aconex.pages;

public class HomePage {

	public static final String oracleLogo = "//span[@class='nav-logo']";

	public static final String mailButton = "//div[@class='bicon ic-CORRESPONDENCE']";
	public static final String blankMailOption = "//div[@id='nav-bar-CORRESPONDENCE-CORRESPONDENCE-CREATEMAIL']";
	public static final String frameName = "//iframe[@id='frameMain']";
	public static final String headerName = "//div[@class='user-wrapper']";
	public static final String mailTitle = "//div[@class='mailThreadHeader-subject ng-binding']";
	public static final String directoryButton = "//table[@id='heroSection']//tr[1][@class='mailMetaRow']//td[3]//div[@class='uiButton-label']";
	public static final String globalOptionLink = "//li[@id='ACONEX_list']";
	public static final String mailTypeDropDown = "//table[@id='typeSection']//tbody//select";
	public static final String mailTypeDropDownOptions = "//select[@id='Correspondence_correspondenceTypeID']/optgroup";
	public static final String option4 = "//tr[@class='mailMetaRow']//optGroup//option[@name='Design Change Notification']";
	public static final String groupName = "//input[@id='FIRST_NAME']";
	public static final String familyName = "//input[@id='LAST_NAME']";
	public static final String searchButton = "//button[@id='btnSearch_page']";
	public static final String selectNamecheckBox = "//input[@name='USERS_LIST']";
	public static final String toButton = "//div[@id='searchResultsContainer']//button[@id='btnAddTo_page']";
	public static final String okBtn = "//button[@id='btnOk']";
	public static final String subjectTextBox = "//input[@id='Correspondence_subject']";
	public static final String subjectTextBox1 = "//div[@class='uiField large isRequired']";

	public static final String sendbutton = "//button[@id='btnSend']";
	public static final String mailtype = "//div[@data-automation-id='mailHeader-type-label']";
	public static final String mailtypeValue = "//div[@class='mailHeader-value']//span[@class='ng-binding ng-scope']";
	public static final String generatedMailNumber = "//div[@data-automation-id='mailHeader-number-value']";
	public static final String generatedReferenceNumber = "//div[@data-automation-id='mailHeader-refNumber-value']";
	public static final String mailTypeActual = "//div[@class='mailHeader-value']//span[@class='ng-binding ng-scope']";

	public static final String userInfo = "//span[@class='nav-details']";
	public static final String logOffButton = "//div[@class='auiPopover-content']//a";
	public static final String logInButton = "//button[@id='btnLogin']";
}
