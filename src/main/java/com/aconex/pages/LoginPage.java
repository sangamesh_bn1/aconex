package com.aconex.pages;

public class LoginPage {

	public static final String userNameTextBox = "//input[@id='userName']";
	public static final String passwordTextBox = "//input[@id='password']";
	public static final String loginBtn = "//button[@id='login']";

}
