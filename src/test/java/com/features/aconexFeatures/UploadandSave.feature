Feature: Document upload and search

Scenario Outline: User uploads document and performs search operation and saves search
    Given User clicks on login
	When User enters valid credentials to login "<Username2>" "<Password2>" "<PageTitle>"
	Then User uploads new document
	Then User searches document
	And  User saves search criteria
	And User logout
	
	Examples:
	|Username2|Password2|PageTitle|
	|Username2|Password2|PageTitle|